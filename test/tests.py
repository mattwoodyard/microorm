import unittest
import sys
import microorm

class TestSQL(unittest.TestCase):


    def test001(self):
        """where clauses should be generate correctly"""
        self.assertEqual(microorm.sql_where_builder({'start':1234, 'end':12345})[0] ,
                """where end = ? and start = ?""")

    def test002(self):
        """empty where clauses should generate correctly"""
        self.assertEqual(microorm.sql_where_builder({})[0] ,"")

    def test003(self):
        """upate should be correct"""
        self.assertEqual(microorm.sql_update_str("test_table", {"id": 1}, {"name": "x", "group":"y", "cue":30}),
            ("""UPDATE test_table SET cue = ?, group = ?, name = ? where id = ?""", [30, "y","x",1]))
    
    def test004(self):
        """empty upate should be correct"""
        self.assertEqual(microorm.sql_update_str("test_table", {"id": 1}, {}),
            ("""UPDATE test_table  where id = ?""", [1]))

    def test005(self):
        """test delete"""
        self.assertEqual(microorm.sql_delete_str("test_table", {"id": 1} ),
            ("""DELETE FROM test_table where id = ?""", [1]))

    def test006(self):
        """test delete"""
        self.assertEqual(microorm.sql_insert_str("test_table", {"id": 1, "name": "x",
            "row_count":12}, ["id"], "id" ),
            ("""INSERT INTO test_table (id,name,row_count)values(?,?,?)""", [1,"x",12]))


    def test007(self):
        """Correctly generate non = where clauses"""
        SQL  = microorm.SQL
        self.assertEqual(microorm.sql_where_builder({'start':SQL.gt(1234), 'end':["<", 12345]})[0] ,
                """where end < ? and start > ?""")

if __name__=="__main__":
    unittest.main()






