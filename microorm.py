


class DBVariant:
    query_param_str="?"
    cursor_last_id = lambda x: x.lastrowid
    


class Operators:
    def eq(self, val): 
        return  ["=" , val]    
    
    def gt(self,v):
        return [">",v]

    def lt(self,v):
        return ["<",v]

SQL = Operators()


def op_or_thru(op, i):
    if isinstance(op,list) or isinstance(op, tuple):
        return op[i]

    return op

def or_eq(op):
    if isinstance(op,list) or isinstance(op, tuple):
        return op[0]

    return "=" 

def sql_where_builder(query_dict, db_variant = DBVariant):
    """Want to do complicated joins and or/and sutff... USE another ORM"""
    flds = sorted(query_dict.keys())
    # XXX- :(
    vals = [op_or_thru(query_dict[k],1) for k in flds]
    flds = [(f, or_eq(query_dict[f])) for f in flds]

    # XXX - add operator handling
    # q : [=,blah], e: [>, x]
    # XXX - support an alternate korma inspired list syntax



    where = " and ".join(["%s %s %s"%(f[0], f[1], db_variant.query_param_str) for f in flds])
    where = "where " + where if len(flds) != 0 else ""+where
    return where, vals


def sql_select_str(table_name, query_dict, db_variant):
    where,vals = sql_where_builder(query_dict, db_variant)
    sql = "SELECT * FROM %s %s"%(table_name, where)
    return (sql, vals)

def query(dbc, table_name, query_dict, db_variant = DBVariant):
    c = dbc.cursor()
    (sql, vals) = sql_select_str(table_name, query_dict, db_variant)
    c.execute(sql,vals)
    rows = c.fetchall() 
    return rows

def get_one(dbc, table_name, query_dict, db_variant = DBVariant):
    rows = query(dbc, table_name, query_dict)

    if len(rows) != 1:
        return ("Invalid Number of rows", None)
    return (None, rows[0])


def sql_update_str( table_name, on_keyed, update_dict, db_variant = DBVariant):
    where, vals = sql_where_builder(on_keyed, db_variant)
   
    skeys = sorted(update_dict.keys())
    update_sets = "" if len(skeys) == 0 else "SET "
    
    update_sets = update_sets + ", ".join(["%s = %s"%(k, db_variant.query_param_str) for k in skeys])
    update_vals = [update_dict[k] for k in skeys]

    sql = "UPDATE %s %s %s"%(table_name, update_sets, where)

    return (sql, update_vals+vals)


def update(dbc, table_name, on_keyed, update_dict, db_variant = DBVariant):
    c = dbc.cursor()
    sql, vals = sql_update_str( table_name, on_keyed, update_dict, db_variant )
    c.execute(sql,  vals)
    # XXX - should return something
    return (None, None)

def sql_delete_str(table_name, query_dict, db_variant = DBVariant):
    where,vals = sql_where_builder(query_dict, db_variant)
    sql = "DELETE FROM %s %s"%(table_name, where)
    return (sql, vals)

def delete(dbc, table_name, query_dict, db_variant = DBVariant):
    c = dbc.cursor()
    sql,vals = sql_delete_str(table_name, query_dict, db_variant)
    c.execute(sql,vals)



def sql_insert_str(table_name, row_dict, keys = [], pk_field ="id", db_variant = DBVariant): 
    flds = sorted(row_dict.keys())
    vals = [row_dict[k] for k in flds]
    vins = ",".join(["?" for f in flds])
    # XXX - table_name is NOT safe

    sql = "INSERT INTO %s (%s)values(%s)"%(table_name, ",".join(flds), vins)
    return (sql, vals)

def insert(dbc, table_name, row_dict, keys = [], pk_field ="id", db_variant = DBVariant): 
    """Performs and SQL insert on the provided values
    
    @param dbc a DBAPI compatible database connection
    @param table_name the name of the table you want to insert the row in
    @param row_dict the dictionary containing the column name ->value mapping to store
    @param keys a list containing the names of keys to use in order to retrieve the newly
                selected object from the db. If keys is empty then the last rowid is used to 
                select 
    @param last_obj a tuple of (key_name, function to retrieve the key value from a database
    cursor

    @return tuple containing (Err, Row) Err will be none if the query completed successfully
    otherwise it will be a value describing the error

    """

    c = dbc.cursor()

    (sql, vals) = sql_insert_str(table_name, row_dict, keys, pk_field, db_variant)
    c.execute(sql, vals)
    dbc.commit()

    kd = dict([(k,row_dict.get(k)) for k in keys if row_dict.get(k) != None])


    if len(kd.values()) == len(keys) and len(keys) != 0:
        return sql_get_one(dbc, table_name, kd, db_variant)
    else:
        return sql_get_one(dbc, table_name, {pk_field:db_variant(c)}, db_variant)



